﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Chip8
{
    static class LogWindow
    {
        private const int PosX = 67;
        private const int PosY = 1;
        private const int Height = 20;
        private const int Width = 30;

        private static int lineIndex;

        public static void WriteLine(string message)
        {
            Console.SetCursorPosition(PosX, PosY + lineIndex);

            var toPrint = message?.Length > Width ? message.Substring(0, Width) : message;
            Console.Write(toPrint);
            lineIndex++;

            if (lineIndex >= Height)
            {
                for (int i = 0; i < Height; i++)
                {
                    Console.SetCursorPosition(PosX, PosY + i);
                    Console.Write(new string(Enumerable.Repeat(' ', Width).ToArray()));
                }

                lineIndex = 0;
            }
        }
    }

    static class Program
    {
        private static void Test()
        {
            new TestCPU().Test();
        }

        private static byte[] ReadProgram()
        {
            byte[] result;
            using (var stream = File.OpenRead("c8games\\PONG2"))
            {
                result = new byte[stream.Length];
                stream.Read(result, 0, result.Length);
            }
            return result;
        }

        public static void Main()
        {
        BEGIN:
            Console.CursorVisible = false;
            Console.Clear();

            Test();

            var chip8 = new CPU();
            chip8.Flash(ReadProgram());

            var gfxBuffer = new byte[chip8.GFX.Length];            

            var timer = new System.Timers.Timer();
            timer.Interval = 10;
            timer.AutoReset = true;
            timer.Enabled = true;
            timer.Elapsed += (sender, args) =>
            {
                chip8.Cycle();

                if (chip8.KeyUsed)
                {
                    for (int i = 0; i < chip8.Key.Length; i++)
                    {
                        chip8.Key[i] = false;
                    }
                }

                if (chip8.ClearScreenFlag)
                {
                    Console.Clear();
                }

                if (chip8.DrawFlag)
                {
                    int x = 0, y = 0;
                    for (int i = 0; i < chip8.GFX.Length; i++)
                    {
                        if (gfxBuffer[i] != chip8.GFX[i])
                        {
                            gfxBuffer[i] = chip8.GFX[i];
                            Console.SetCursorPosition(x, y);
                            if (chip8.GFX[i] == 1)
                                Console.Write("#");
                            else
                                Console.Write(" ");
                        }

                        x++;

                        if (x != 64) continue;
                        x = 0;
                        y++;
                    }
                }

                if (Console.KeyAvailable)
                {
                    Console.SetCursorPosition(0, 0);
                    var key = Console.ReadKey();
                    chip8.Key[0] = key.Key == ConsoleKey.NumPad0;
                    chip8.Key[1] = key.Key == ConsoleKey.NumPad1;
                    chip8.Key[2] = key.Key == ConsoleKey.NumPad2;
                    chip8.Key[3] = key.Key == ConsoleKey.NumPad3;
                    chip8.Key[4] = key.Key == ConsoleKey.NumPad4;
                    chip8.Key[5] = key.Key == ConsoleKey.NumPad5;
                    chip8.Key[6] = key.Key == ConsoleKey.NumPad6;
                    chip8.Key[7] = key.Key == ConsoleKey.NumPad7;
                    chip8.Key[8] = key.Key == ConsoleKey.NumPad8;
                    chip8.Key[9] = key.Key == ConsoleKey.NumPad9;
                    chip8.Key[10] = key.Key == ConsoleKey.A;
                    chip8.Key[11] = key.Key == ConsoleKey.S;
                    chip8.Key[12] = key.Key == ConsoleKey.D;
                    chip8.Key[13] = key.Key == ConsoleKey.F;
                    chip8.Key[14] = key.Key == ConsoleKey.G;
                    chip8.Key[15] = key.Key == ConsoleKey.H;
                }
            };

            while (true)
            {
                Thread.Sleep(1000);
            }
        }
    }
}
