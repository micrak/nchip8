﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Chip8
{
    public class TestCPU
    {
        CPU cpu;

        void Init()
        {
            this.cpu = new CPU();
        }

        public void Test()
        {
            this.Test_Cycle_ResetsFlags();
            this.Test_Cycle_DecreaseTimers();
            this.Test_00E0_ClearsTheScreen();
            this.Test_00EE_Returns_from_a_subroutine();
            this.Test_1NNN_Jumps_to_address_NNN();
            this.Test_2NNN_Calls_subroutine_at_NNN();
            this.Test_3XNN_Skips_the_next_instruction_if_VX_equals_NN();
            this.Test_4XNN_Skips_the_next_instruction_if_VX_doesnt_equal_NN();
            this.Test_5XY0_Skips_the_next_instruction_if_VX_equals_VY();
            this.Test_6XNN_Sets_VX_to_NN();
            this.Test_7XNN_Adds_NN_to_VX();
            this.Test_8XY0_Sets_VX_to_the_value_of_VY();
            this.Test_8XY1_Sets_VX_to_VX_or_VY_VF_is_reset_to_0();
            this.Test_8XY2_Sets_VX_to_VX_and_VY_VF_is_reset_to_0();
            this.Test_8XY3_Sets_VX_to_VX_xor_VY_VF_is_reset_to_0();
            this.Test_8XY4_Adds_VY_to_VX_VF_is_set_to_1_when_theres_a_carry_and_to_0_when_there_isnt();
            this.Test_8XY5_VY_is_subtracted_from_VX_VF_is_set_to_0_when_theres_a_borrow_and_1_when_there_isnt();
            this.Test_8XY6_Shifts_VX_right_by_one_VF_is_set_to_the_value_of_the_least_significant_bit_of_VX_before_the_shift();
            this.Test_8XY7_Sets_VX_to_VY_minus_VX_VF_is_set_to_0_when_theres_a_borrow_and_1_when_there_isnt();
            this.Test_8XYE_Shifts_VX_left_by_one_VF_is_set_to_the_value_of_the_most_significant_bit_of_VX_before_the_shift();
            this.Test_9XY0_Skips_the_next_instruction_if_VX_doesnt_equal_VY();
            this.Test_ANNN_Sets_I_to_the_address_NNN();
            this.Test_BNNN_Jumps_to_the_address_NNN_plus_V0();
            this.Test_CXNN_Sets_VX_to_the_result_of_a_bitwise_and_operation_on_a_random_number_iTypically_0_to_255i_and_NN();
            this.Test_DXYN_Draws_a_sprite();
            this.Test_EX9E_Skips_the_next_instruction_if_the_key_stored_in_VX_is_pressed();
            this.Test_EXA1_Skips_the_next_instruction_if_the_key_stored_in_VX_isnt_pressed();
            this.Test_FX07_Sets_VX_to_the_value_of_the_delay_timer();
            this.Test_FX0A_A_key_press_is_awaited_and_then_stored_in_VX();
            this.Test_FX15_Sets_the_delay_timer_to_VX();
            this.Test_FX18_Sets_the_sound_timer_to_VX();
            this.Test_FX1E_Adds_VX_to_I();
            this.Test_FX29_Sets_I_to_the_location_of_the_sprite_for_the_character_in_VX_Characters_0_F_iin_hexadecimali_are_represented_by_a_4x5_font();
            this.Test_FX33_Stores_the_Binary_coded_decimal_representation_of_VX_at_the_addresses_I_I_plus_1_and_I_plus_2();
            this.Test_FX55_Stores_V0_to_VX_iincluding_VXi_in_memory_starting_at_address_I();
            this.Test_FX65_Fills_V0_to_VX_including_VX_with_values_from_memory_starting_at_address_I();
        }

        private void Test_Cycle_DecreaseTimers()
        {            
            this.Init();

            this.cpu.Flash(new byte[]
            {
                0xF0, 0x1E,
                0xF0, 0x1E,
                0xF0, 0x1E,
                0xF0, 0x1E,
                0xF0, 0x1E,
                0xF0, 0x1E,
            });

            this.cpu.SoundTimer = 5;
            this.cpu.DelayTimer = 5;

            this.cpu.Cycle();
            Assert(() => this.cpu.SoundTimer == 4);
            Assert(() => this.cpu.DelayTimer == 4);

            this.cpu.Cycle();
            Assert(() => this.cpu.SoundTimer == 3);
            Assert(() => this.cpu.DelayTimer == 3);

            this.cpu.Cycle();
            Assert(() => this.cpu.SoundTimer == 2);
            Assert(() => this.cpu.DelayTimer == 2);

            this.cpu.Cycle();
            Assert(() => this.cpu.SoundTimer == 1);
            Assert(() => this.cpu.DelayTimer == 1);

            this.cpu.Cycle();
            Assert(() => this.cpu.SoundTimer == 0);
            Assert(() => this.cpu.DelayTimer == 0);

            this.cpu.Cycle();
            Assert(() => this.cpu.SoundTimer == 0);
            Assert(() => this.cpu.DelayTimer == 0);
        }

        private void Test_FX65_Fills_V0_to_VX_including_VX_with_values_from_memory_starting_at_address_I()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0xF7, 0x65
            });

            for (byte i = 0; i < 0xF; i++)
            {
                this.cpu.Mem[0x500 + i] = i;
            }

            this.cpu.I = 0x500;

            this.cpu.Cycle();

            Assert(() => this.cpu.PC == 0x200 + 2);

            for (byte i = 0; i <= 7; i++)
            {
                Assert(() => this.cpu.V[i] == i);
            }
        }

        private void Test_FX55_Stores_V0_to_VX_iincluding_VXi_in_memory_starting_at_address_I()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0xF7, 0x55
            });

            for (byte i = 0; i < 0xF; i++)
            {
                this.cpu.V[i] = i;
            }

            this.cpu.I = 0x500;
            this.cpu.Cycle();
            Assert(() => this.cpu.PC == 0x200 + 2);

            for (byte i = 0; i <= 7; i++)
            {
                Assert(() => this.cpu.Mem[0x500 + i] == i);
            }

            Assert(() => this.cpu.Mem[0x508] == 0);
        }

        private void Test_FX33_Stores_the_Binary_coded_decimal_representation_of_VX_at_the_addresses_I_I_plus_1_and_I_plus_2()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0xF7, 0x33
            });

            this.cpu.V[7] = 0xFF;
            this.cpu.I = 0x500;
            this.cpu.Cycle();

            Assert(() => this.cpu.Mem[0x500] == 2);
            Assert(() => this.cpu.Mem[0x501] == 5);
            Assert(() => this.cpu.Mem[0x502] == 5);
            Assert(() => this.cpu.PC == 0x200 + 2);
        }

        private void Test_FX29_Sets_I_to_the_location_of_the_sprite_for_the_character_in_VX_Characters_0_F_iin_hexadecimali_are_represented_by_a_4x5_font()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0xF7, 0x29
            });
            this.cpu.V[7] = 3;
            this.cpu.I = 0;
            this.cpu.Cycle();
            Assert(() => this.cpu.I == 15);
            Assert(() => this.cpu.PC == 0x200 + 2);
        }

        private void Test_FX1E_Adds_VX_to_I()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0xF7, 0x1E
            });
            this.cpu.I = 0x07;
            this.cpu.V[7] = 0x01;
            this.cpu.Cycle();
            Assert(() => this.cpu.I == 0x08);
            Assert(() => this.cpu.PC == 0x200 + 2);
        }

        private void Test_FX18_Sets_the_sound_timer_to_VX()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0xF7, 0x18
            });

            this.cpu.SoundTimer = 0;
            this.cpu.V[7] = 11;
            this.cpu.Cycle();
            Assert(() => this.cpu.SoundTimer == 10);
            Assert(() => this.cpu.PC == 0x200 + 2);
        }

        private void Test_FX15_Sets_the_delay_timer_to_VX()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0xF5, 0x15
            });

            this.cpu.DelayTimer = 0;
            this.cpu.V[5] = 7;
            this.cpu.Cycle();
            Assert(() => this.cpu.DelayTimer == 6);
            Assert(() => this.cpu.PC == 0x200 + 2);
        }

        private void Test_FX0A_A_key_press_is_awaited_and_then_stored_in_VX()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0xF1, 0x0A,
                0x00, 0x00
            });

            this.cpu.Cycle();
            this.cpu.Cycle();
            this.cpu.Cycle();
            Assert(() => this.cpu.PC == 0x200);

            this.cpu.Key[4] = true;
            this.cpu.Cycle();
            Assert(() => this.cpu.PC == 0x200 + 2);
            Assert(() => this.cpu.V[1] == 4);
        }

        private void Test_FX07_Sets_VX_to_the_value_of_the_delay_timer()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0xF0, 0x07
            });

            this.cpu.V[0] = 0;
            this.cpu.DelayTimer = 9;
            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == 9);
            Assert(() => this.cpu.PC == 0x200 + 2);
        }

        private void Test_EXA1_Skips_the_next_instruction_if_the_key_stored_in_VX_isnt_pressed()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0xE0, 0xA1,
                0xE0, 0xA1,
                0x00, 0x00,
                0x00, 0x00,
            });

            this.cpu.V[0] = 5;
            this.cpu.Key[5] = true;
            this.cpu.Cycle();
            Assert(() => this.cpu.PC == 0x200 + 1 * 2);

            this.cpu.V[0] = 5;
            this.cpu.Key[5] = false;
            this.cpu.Cycle();
            Assert(() => this.cpu.PC == 0x200 + 3 * 2);
        }

        private void Test_EX9E_Skips_the_next_instruction_if_the_key_stored_in_VX_is_pressed()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0xE0, 0x9E,
                0xE0, 0x9E,
                0x00, 0x00,
                0x00, 0x00,
            });

            this.cpu.V[0] = 5;
            this.cpu.Key[5] = false;
            this.cpu.Cycle();
            Assert(() => this.cpu.PC == 0x200 + 1 * 2);

            this.cpu.V[0] = 5;
            this.cpu.Key[5] = true;
            this.cpu.Cycle();
            Assert(() => this.cpu.PC == 0x200 + 3 * 2);
        }

        private void Test_DXYN_Draws_a_sprite()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0xD0, 0x11,
                0xD0, 0x11,
                0xD0, 0x11,
                0xD0, 0x11,
                0xD0, 0x11,
                0xFF, 0xFF
            });

            this.cpu.V[0] = 0;
            this.cpu.V[1] = 0;
            this.cpu.I = 0x200 + 5 * 2;

            this.cpu.Cycle();
            for (int i = 0; i < 8; i++)
                Assert(() => this.cpu.GFX[i] == 1);

            this.cpu.Cycle();
            for (int i = 0; i < 8; i++)
                Assert(() => this.cpu.GFX[i] == 0);
        }

        private void Test_Cycle_ResetsFlags()
        {
            this.Init();
            this.cpu.Reset();

            this.cpu.DrawFlag = true;
            this.cpu.ClearScreenFlag = true;

            this.cpu.Cycle();

            Assert(() => this.cpu.DrawFlag == false);
            Assert(() => this.cpu.ClearScreenFlag == false);
        }

        private void Test_CXNN_Sets_VX_to_the_result_of_a_bitwise_and_operation_on_a_random_number_iTypically_0_to_255i_and_NN()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0xC1, 0xCD,
                0xC2, 0x3F,
            });

            this.cpu.NextRandom = 0xA;

            this.cpu.Cycle();
            Assert(() => this.cpu.V[1] == (byte)(0xA & 0xCD));

            this.cpu.Cycle();
            Assert(() => this.cpu.V[2] == (byte)(0xA & 0x3F));
        }

        private void Test_BNNN_Jumps_to_the_address_NNN_plus_V0()
        {
            this.Init();
            
            this.cpu.Flash(new byte[]
            {
                0xB1, 0x11
            });

            this.cpu.V[0] = 0x07;
            this.cpu.Cycle();

            Assert(() => this.cpu.PC == 0x0111 + 0x0007);
        }

        private void Test_ANNN_Sets_I_to_the_address_NNN()
        {
            this.Init();

            this.cpu.Flash(new byte[]
            {
                0xA1, 0x11
            });

            this.cpu.Cycle();

            Assert(() => this.cpu.I == 0x0111);
        }

        private void Test_9XY0_Skips_the_next_instruction_if_VX_doesnt_equal_VY()
        {
            this.Init();

            this.cpu.Flash(new byte[]
            {
                0x90, 0x10,
                0x90, 0x10,
                0x90, 0x10,
                0x90, 0x10,
            });

            this.cpu.V[0] = 1;
            this.cpu.V[1] = 1;
            this.cpu.Cycle();
            Assert(() => this.cpu.PC == 0x200 + 1 * 2);

            this.cpu.V[0] = 1;
            this.cpu.V[1] = 0;
            this.cpu.Cycle();
            Assert(() => this.cpu.PC == 0x200 + 3 * 2);
        }

        private void Test_8XYE_Shifts_VX_left_by_one_VF_is_set_to_the_value_of_the_most_significant_bit_of_VX_before_the_shift()
        {
            this.Init();

            this.cpu.Flash(new byte[]
            {
                0x80, 0x1E,
                0x80, 0x1E,
                0x80, 0x1E,
                0x80, 0x1E,
            });

            this.cpu.V[0] = 0x7F;
            this.cpu.V[0xF] = 0;
            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == 0xFE);
            Assert(() => this.cpu.V[0xF] == 0);

            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == 0xFC);
            Assert(() => this.cpu.V[0xF] == 1);

            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == 0xF8);
            Assert(() => this.cpu.V[0xF] == 1);

            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == 0xF0);
            Assert(() => this.cpu.V[0xF] == 1);
        }

        private void Test_8XY7_Sets_VX_to_VY_minus_VX_VF_is_set_to_0_when_theres_a_borrow_and_1_when_there_isnt()
        {
            this.Init();

            this.cpu.Flash(new byte[]
            {
                0x80, 0x17,
                0x80, 0x17,
                0x80, 0x17,
            });

            this.cpu.V[0] = 1;
            this.cpu.V[1] = 7;
            this.cpu.V[0xF] = 0;
            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == 6);
            Assert(() => this.cpu.V[1] == 7);
            Assert(() => this.cpu.V[0xF] == 1);

            this.cpu.V[0] = 7;
            this.cpu.V[1] = 7;
            this.cpu.V[0xF] = 0;
            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == 0);
            Assert(() => this.cpu.V[1] == 7);
            Assert(() => this.cpu.V[0xF] == 1);

            this.cpu.V[0] = 8;
            this.cpu.V[1] = 7;
            this.cpu.V[0xF] = 0;
            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == 0xFF);
            Assert(() => this.cpu.V[1] == 7);
            Assert(() => this.cpu.V[0xF] == 0);
        }

        private void Test_8XY6_Shifts_VX_right_by_one_VF_is_set_to_the_value_of_the_least_significant_bit_of_VX_before_the_shift()
        {
            this.Init();

            this.cpu.V[0] = 0xFE;

            this.cpu.Flash(new byte[]
            {
                0x80, 0x06,
                0x80, 0x06,
                0x80, 0x06,
                0x80, 0x06,
            });

            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == (byte)(0xFE >> 1));
            Assert(() => this.cpu.V[0xF] == 0);

            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == (byte)(0xFE >> 2));
            Assert(() => this.cpu.V[0xF] == 1);

            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == (byte)(0xFE >> 3));
            Assert(() => this.cpu.V[0xF] == 1);

            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == (byte)(0xFE >> 4));
            Assert(() => this.cpu.V[0xF] == 1);
        }

        private void Test_8XY5_VY_is_subtracted_from_VX_VF_is_set_to_0_when_theres_a_borrow_and_1_when_there_isnt()
        {
            this.Init();

            this.cpu.Flash(new byte[]
            {
                0x80, 0x15,
                0x80, 0x15,
                0x80, 0x15,
                0x80, 0x15,
            });

            this.cpu.V[0] = 0;
            this.cpu.V[1] = 0;
            this.cpu.V[0xF] = 0;
            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == 0);
            Assert(() => this.cpu.V[1] == 0);
            Assert(() => this.cpu.V[0xF] == 1);

            this.cpu.V[0] = 0xFF;
            this.cpu.V[1] = 0xFF;
            this.cpu.V[0xF] = 0;
            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == 0);
            Assert(() => this.cpu.V[1] == 0xFF);
            Assert(() => this.cpu.V[0xF] == 1);

            this.cpu.V[0] = 0x01;
            this.cpu.V[1] = 0x02;
            this.cpu.V[0xF] = 0;
            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == 0xFF);
            Assert(() => this.cpu.V[1] == 0x02);
            Assert(() => this.cpu.V[0xF] == 0);

            this.cpu.V[0] = 0x01;
            this.cpu.V[1] = 0xFF;
            this.cpu.V[0xF] = 0;
            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == 0x02);
            Assert(() => this.cpu.V[1] == 0xFF);
            Assert(() => this.cpu.V[0xF] == 0);
        }

        private void Test_8XY4_Adds_VY_to_VX_VF_is_set_to_1_when_theres_a_carry_and_to_0_when_there_isnt()
        {
            this.Init();

            this.cpu.Flash(new byte[]
            {
                0x80, 0x14,
                0x80, 0x14,
                0x80, 0x14,
            });

            this.cpu.V[0] = 7;
            this.cpu.V[1] = 11;
            this.cpu.V[0xF] = 1;
            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == 18);
            Assert(() => this.cpu.V[0xF] == 0);
            Assert(() => this.cpu.PC == 0x200 + 1 * 2);

            this.cpu.V[0] = 254;
            this.cpu.V[1] = 1;
            this.cpu.V[0xF] = 1;
            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == 255);
            Assert(() => this.cpu.V[0xF] == 0);
            Assert(() => this.cpu.PC == 0x200 + 2 * 2);

            this.cpu.V[0] = 0xFF;
            this.cpu.V[1] = 1;
            this.cpu.V[0xF] = 1;
            this.cpu.Cycle();
            Assert(() => this.cpu.V[0] == 0);
            Assert(() => this.cpu.V[0xF] == 1);
            Assert(() => this.cpu.PC == 0x200 + 3 * 2);
        }

        private void Test_8XY3_Sets_VX_to_VX_xor_VY_VF_is_reset_to_0()
        {
            this.Init();

            this.cpu.V[7] = 0x4;
            this.cpu.V[8] = 0x5;
            this.cpu.V[0xF] = 1;

            this.cpu.Flash(new byte[]
            {
                0x87, 0x83
            });

            this.cpu.Cycle();

            Assert(() => this.cpu.V[7] == (0x4 ^ 0x5));
            Assert(() => this.cpu.V[0xF] == 0);
        }

        private void Test_8XY2_Sets_VX_to_VX_and_VY_VF_is_reset_to_0()
        {
            this.Init();

            this.cpu.V[7] = 0x4;
            this.cpu.V[8] = 0x5;
            this.cpu.V[0xF] = 1;

            this.cpu.Flash(new byte[]
            {
                0x87, 0x82
            });

            this.cpu.Cycle();

            Assert(() => this.cpu.V[7] == (0x4 & 0x5));
            Assert(() => this.cpu.V[0xF] == 0);
        }

        private void Test_8XY1_Sets_VX_to_VX_or_VY_VF_is_reset_to_0()
        {
            this.Init();

            this.cpu.V[7] = 0x4;
            this.cpu.V[8] = 0x5;
            this.cpu.V[0xF] = 1;

            this.cpu.Flash(new byte[]
            {
                0x87, 0x81
            });

            this.cpu.Cycle();

            Assert(() => this.cpu.V[7] == (0x4 | 0x5));
            Assert(() => this.cpu.V[0xF] == 0);
        }

        private void Test_8XY0_Sets_VX_to_the_value_of_VY()
        {
            this.Init();

            this.cpu.V[0] = 0;
            this.cpu.V[7] = 7;

            this.cpu.Flash(new byte[]
            {
                0x80, 0x70
            });

            this.cpu.Cycle();

            Assert(() => this.cpu.V[7] == 7);
        }

        private void Test_7XNN_Adds_NN_to_VX()
        {
            this.Init();

            this.cpu.Flash(new byte[]
            {
                0x70, 0x01,
                0x70, 0x02,
                0x70, 0x07,
                0x7F, 0x02,
                0x7F, 0x03,
                0x7F, 0x08,
            });

            for (int i = 0; i < 6; i++)
            {
                this.cpu.Cycle();
            }

            Assert(() => this.cpu.V[0x0] == 0x01 + 0x02 + 0x07);
            Assert(() => this.cpu.V[0xF] == 0x02 + 0x03 + 0x08);
        }

        private void Test_6XNN_Sets_VX_to_NN()
        {
            this.Init();

            this.cpu.Flash(new byte[]
            {
                0x60, 0x01,
                0x61, 0x02,
                0x67, 0x03,
                0x6A, 0x44,
                0x6E, 0x55,
                0x6F, 0xFF,
            });

            this.cpu.Cycle();
            this.cpu.Cycle();
            this.cpu.Cycle();
            this.cpu.Cycle();
            this.cpu.Cycle();
            this.cpu.Cycle();

            Assert(() => this.cpu.V[0x0] == 0x01);
            Assert(() => this.cpu.V[0x1] == 0x02);
            Assert(() => this.cpu.V[0x7] == 0x03);
            Assert(() => this.cpu.V[0xA] == 0x44);
            Assert(() => this.cpu.V[0xE] == 0x55);
            Assert(() => this.cpu.V[0xF] == 0xFF);
        }

        private void Test_5XY0_Skips_the_next_instruction_if_VX_equals_VY()
        {
            this.Init();
            this.cpu.V[0] = 7;
            this.cpu.V[1] = 11;

            this.cpu.Flash(new byte[]
            {
                0x50, 0x10,
                0x50, 0x10,
                0x00, 0x00,
                0x00, 0x00
            });

            this.cpu.Cycle();
            Assert(() => this.cpu.PC == 0x200 + 1 * 2);

            this.cpu.V[0] = 11;
            this.cpu.V[1] = 11;

            this.cpu.Cycle();
            Assert(() => this.cpu.PC == 0x200 + 3 * 2);
        }

        private void Test_4XNN_Skips_the_next_instruction_if_VX_doesnt_equal_NN()
        {
            this.Init();
            this.cpu.V[0] = 1;
            this.cpu.Flash(new byte[]
            {
                0x40, 0x77,
                0x00, 0x00,
                0x40, 0x01,
                0x00, 0x00
            });

            this.cpu.Cycle();
            Assert(() => this.cpu.PC == 0x200 + 2 * 2);

            this.cpu.Cycle();
            Assert(() => this.cpu.PC == 0x200 + 3 * 2);
        }

        private void Test_3XNN_Skips_the_next_instruction_if_VX_equals_NN()
        {
            this.Init();
            this.cpu.V[0] = 1;
            this.cpu.Flash(new byte[]
            {
                0x30, 0x77,
                0x30, 0x01,
            });

            this.cpu.Cycle();
            Assert(() => this.cpu.PC == 0x200 + 2);

            this.cpu.Cycle();
            Assert(() => this.cpu.PC == 0x200 + 6);
        }

        private void Test_2NNN_Calls_subroutine_at_NNN()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0x21, 0x23
            });

            Assert(() => this.cpu.StackPointer == 0);
            this.cpu.Cycle();

            Assert(() => this.cpu.StackPointer == 1);
            Assert(() => this.cpu.Stack[0] == 0x200);
            Assert(() => this.cpu.PC == 0x0123);
        }

        private void Test_1NNN_Jumps_to_address_NNN()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0x12, 0x34
            });
            this.cpu.Cycle();

            Assert(() => this.cpu.PC == 0x234);
        }

        private void Test_00E0_ClearsTheScreen()
        {
            this.Init();
            Assert(() => cpu.ClearScreenFlag == false);
            this.cpu.Flash(new byte[]
            {
                0x00, 0xE0
            });
            this.cpu.Cycle();
            Assert(() => cpu.ClearScreenFlag == true);
        }

        private void Test_00EE_Returns_from_a_subroutine()
        {
            this.Init();
            this.cpu.Flash(new byte[]
            {
                0x00, 0xEE
            });

            cpu.StackPointer = 1;
            cpu.Stack[0] = 0x400;

            this.cpu.Cycle();

            Assert(() => cpu.StackPointer == 0);
            Assert(() => cpu.PC == 0x402);
        }

        private static void Assert(Expression<Func<bool>> expression)
        {
            if (!expression.Compile()())
            {
                throw new Exception($"Assertion failed: {expression}");
            }
        }
    }
}
