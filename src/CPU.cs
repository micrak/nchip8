﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chip8
{
    public class CPU
    {
        private static readonly byte[] FontSet =
        {
            0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
            0x20, 0x60, 0x20, 0x20, 0x70, // 1
            0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
            0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
            0x90, 0x90, 0xF0, 0x10, 0x10, // 4
            0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
            0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
            0xF0, 0x10, 0x20, 0x40, 0x40, // 7
            0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
            0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
            0xF0, 0x90, 0xF0, 0x90, 0x90, // A
            0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
            0xF0, 0x80, 0x80, 0x80, 0xF0, // C
            0xE0, 0x90, 0x90, 0x90, 0xE0, // D
            0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
            0xF0, 0x80, 0xF0, 0x80, 0x80  // F
        };

        private readonly Random random = new Random();

        public byte[] Mem;
        public byte[] V;
        public ushort I;
        public ushort PC;
        public byte[] GFX;
        public byte DelayTimer;
        public byte SoundTimer;
        public bool DrawFlag;
        public bool ClearScreenFlag;
        public ushort[] Stack;
        public ushort StackPointer;
        public bool[] Key;
        public byte? NextRandom;
        public bool KeyUsed;

        public Action<string> Debug;

        public CPU()
        {
            this.Reset();
        }

        public void Flash(byte[] program)
        {
            for (int i = 0; i < program.Length; i++)
            {
                var offset = 512 + i;
                if (offset > this.Mem.Length)
                {
                    return;
                }

                this.Mem[offset] = program[i];
            }
        }

        public void Reset()
        {
            this.Mem = new byte[0x1000];
            this.V = new byte[16];
            this.I = 0;
            this.PC = 0x200;
            this.GFX = new byte[64 * 32];
            this.DelayTimer = 0;
            this.SoundTimer = 0;
            this.DrawFlag = false;
            this.ClearScreenFlag = false;
            this.Stack = new ushort[16];
            this.StackPointer = 0;
            this.Key = new bool[16];
            this.NextRandom = null;

            for (int i = 0; i < FontSet.Length; i++)
            {
                this.Mem[i] = FontSet[i];
            }
        }

        public void Cycle()
        {
            this.DrawFlag = false;
            this.ClearScreenFlag = false;
            this.KeyUsed = false;

            var opCode = new OpCode(this.Mem[this.PC], this.Mem[this.PC + 1]);
            this.Debug?.Invoke($"0x{opCode.StringValue.ToUpper()}");
            byte X, NN, N, Y;
            ushort NNN, PIXEL, TEMP;
            bool CARRY;

            switch (opCode.HH)
            {
                case 0x0:
                    switch (opCode.Value)
                    {
                        // 00E0	Display	disp_clear()	Clears the screen.
                        case 0x00E0:
                            this.ClearScreenFlag = true;
                            this.PC += 2;
                            break;

                        // 00EE Flow    return; Returns from a subroutine.
                        case 0x00EE:
                            this.StackPointer--;
                            this.PC = this.Stack[this.StackPointer];
                            this.PC += 2;
                            break;
                    }
                    break;

                // 1NNN	Flow	goto NNN;	Jumps to address NNN.
                case 0x1:
                    NNN = opCode.GetShort(0x0FFF, 0);
                    this.PC = NNN;
                    break;

                // 2NNN	Flow	*(0xNNN)()	Calls subroutine at NNN.
                case 0x2:
                    this.Stack[this.StackPointer] = this.PC;
                    this.StackPointer++;
                    NNN = opCode.GetShort(0x0FFF, 0);
                    this.PC = NNN;
                    break;

                // 3XNN Cond    if (Vx == NN) Skips the next instruction if VX equals NN. (Usually the next instruction is a jump to skip a code block)
                case 0x3:
                    X = opCode.GetByte(0x0F00, 8);
                    NN = opCode.GetByte(0x00FF, 0);
                    if (this.V[X] == NN)
                    {
                        this.PC += 4;
                    }
                    else
                    {
                        this.PC += 2;
                    }

                    break;

                // 4XNN	Cond	if(Vx!=NN)	Skips the next instruction if VX doesn't equal NN. (Usually the next instruction is a jump to skip a code block)
                case 0x4:
                    X = opCode.GetByte(0x0F00, 8);
                    NN = opCode.GetByte(0x00FF, 0);

                    if (this.V[X] != NN)
                    {
                        this.PC += 4;
                    }
                    else
                    {
                        this.PC += 2;
                    }

                    break;

                // 5XY0	Cond	if(Vx==Vy)	Skips the next instruction if VX equals VY. (Usually the next instruction is a jump to skip a code block)
                case 0x5:
                    X = opCode.GetByte(0x0F00, 8);
                    Y = opCode.GetByte(0x00F0, 4);

                    if (this.V[X] == this.V[Y])
                    {
                        this.PC += 4;
                    }
                    else
                    {
                        this.PC += 2;
                    }

                    break;

                // 6XNN	Const	Vx = NN	Sets VX to NN.
                case 0x6:
                    X = opCode.GetByte(0x0F00, 8);
                    NN = opCode.GetByte(0x00FF, 0);
                    this.V[X] = NN;
                    this.PC += 2;
                    break;

                // 7XNN	Const	Vx += NN	Adds NN to VX.
                case 0x7:
                    X = opCode.GetByte(0x0F00, 8);
                    NN = opCode.GetByte(0x00FF, 0);
                    this.V[X] += NN;
                    this.PC += 2;
                    break;

                case 0x8:
                    X = opCode.GetByte(0x0F00, 8);
                    Y = opCode.GetByte(0x00F0, 4);
                    switch (opCode.LL)
                    {
                        // 8XY0	Assign	Vx=Vy	Sets VX to the value of VY.
                        case 0:
                            this.V[X] = this.V[Y];
                            this.PC += 2;
                            break;

                        // 8XY1	BitOp	Vx=Vx|Vy	Sets VX to VX or VY. (Bitwise OR operation) VF is reset to 0.
                        case 1:
                            this.V[X] = (byte)(this.V[X] | this.V[Y]);
                            this.V[0xF] = 0;
                            this.PC += 2;
                            break;

                        // 8XY2	BitOp	Vx=Vx&Vy	Sets VX to VX and VY. (Bitwise AND operation) VF is reset to 0.
                        case 2:
                            this.V[X] = (byte)(this.V[X] & this.V[Y]);
                            this.V[0xF] = 0;
                            this.PC += 2;
                            break;

                        // 8XY3	BitOp	Vx=Vx^Vy	Sets VX to VX xor VY. VF is reset to 0.
                        case 3:
                            this.V[X] = (byte)(this.V[X] ^ this.V[Y]);
                            this.V[0xF] = 0;
                            this.PC += 2;
                            break;

                        // 8XY4	Math	Vx += Vy	Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't.
                        case 4:
                            this.V[X] = Sum(this.V[X], this.V[Y], out CARRY);
                            this.V[0xF] = (byte)(CARRY ? 1 : 0);
                            this.PC += 2;
                            break;

                        // 8XY5	Math	Vx -= Vy	VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
                        case 5:
                            this.V[X] = Subtract(this.V[X], this.V[Y], out CARRY);
                            this.V[0xF] = (byte)(CARRY ? 0 : 1);
                            this.PC += 2;
                            break;

                        // 8XY6	BitOp	Vx >> 1	Shifts VX right by one. VF is set to the value of the least significant bit of VX before the shift.[2]
                        case 6:
                            this.V[0xF] = (byte)(this.V[X] & 0x01);
                            this.V[X] >>= 1;
                            this.PC += 2;
                            break;

                        // 8XY7	Math	Vx=Vy-Vx	Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
                        case 7:
                            this.V[X] = Subtract(this.V[Y], this.V[X], out CARRY);
                            this.V[0xF] = (byte)(CARRY ? 0 : 1);
                            this.PC += 2;
                            break;

                        // 8XYE	BitOp	Vx << 1	Shifts VX left by one. VF is set to the value of the most significant bit of VX before the shift.[2]
                        case 0xE:
                            this.V[0xF] = (byte)((this.V[X] & 0x80) >> 7);
                            this.V[X] <<= 1;
                            this.PC += 2;
                            break;
                    }

                    break;

                // 9XY0	Cond	if(Vx!=Vy)	Skips the next instruction if VX doesn't equal VY. (Usually the next instruction is a jump to skip a code block)
                case 0x9:
                    X = opCode.GetByte(0x0F00, 8);
                    Y = opCode.GetByte(0x00F0, 4);

                    if (this.V[X] != this.V[Y])
                    {
                        this.PC += 4;
                    }
                    else
                    {
                        this.PC += 2;
                    }

                    break;

                // ANNN	MEM	I = NNN	Sets I to the address NNN.
                case 0xA:
                    NNN = opCode.GetShort(0x0FFF, 0);
                    this.I = NNN;
                    this.PC += 2;
                    break;

                // BNNN	Flow	PC=V0+NNN	Jumps to the address NNN plus V0.
                case 0xB:
                    NNN = opCode.GetShort(0x0FFF, 0);
                    this.PC = (ushort)(NNN + this.V[0]);
                    break;

                // CXNN Rand    Vx = rand() & NN    Sets VX to the result of a bitwise and operation on a random number (Typically: 0 to 255) and NN.
                case 0xC:
                    X = opCode.GetByte(0x0F00, 8);
                    NN = opCode.GetByte(0x00FF, 0);

                    TEMP = (ushort)random.Next(0, 255);

                    // For tests only.
                    if (this.NextRandom != null)
                    {
                        TEMP = this.NextRandom.Value;
                    }

                    this.V[X] = (byte)(NN & TEMP);

                    this.PC += 2;
                    break;

                // DXYN Disp    draw(Vx, Vy, N)   
                // Draws a sprite at coordinate(VX, VY) that has a 
                // width of 8 pixels and a height of N pixels.Each row 
                // of 8 pixels is read as bit - coded starting from memory location I; 
                // I value doesn’t change after the execution of this instruction.
                // As described above, VF is set to 1 if any screen pixels are flipped 
                // from set to unset when the sprite is drawn, 
                // and to 0 if that doesn’t happen.
                case 0xD:
                    X = this.V[opCode.GetByte(0x0F00, 8)];
                    Y = this.V[opCode.GetByte(0x00F0, 4)];
                    N = opCode.GetByte(0x000F, 0);

                    V[0xF] = 0;
                    for (int yline = 0; yline < N; yline++)
                    {
                        PIXEL = this.Mem[I + yline];
                        for (int xline = 0; xline < 8; xline++)
                        {
                            if ((PIXEL & (0x80 >> xline)) != 0)
                            {
                                var index = X + xline + (Y + yline) * 64;
                                if (index > this.GFX.Length)
                                {
                                    continue;
                                }

                                if (this.GFX[index] == 1)
                                    V[0xF] = 1;
                                this.GFX[index] ^= 1;
                            }
                        }
                    }

                    this.DrawFlag = true;
                    this.PC += 2;

                    break;

                case 0xE:
                    X = opCode.GetByte(0x0F00, 8);
                    switch (opCode.L)
                    {
                        // EX9E	KeyOp	if(key()==Vx)	Skips the next instruction if the key stored in VX is pressed. (Usually the next instruction is a jump to skip a code block)    
                        case 0x9E:
                            if (this.Key[this.V[X]] == true)
                            {
                                this.PC += 4;
                                this.KeyUsed = true;
                            }
                            else
                            {
                                this.PC += 2;
                            }                            

                            break;

                        // EXA1	KeyOp	if(key()!=Vx)	Skips the next instruction if the key stored in VX isn't pressed. (Usually the next instruction is a jump to skip a code block)
                        case 0xA1:
                            if (this.Key[this.V[X]] != true)
                            {
                                this.PC += 4;
                            }
                            else
                            {
                                this.PC += 2;
                                this.KeyUsed = true;
                            }                            

                            break;
                    }
                    break;

                case 0xF:
                    X = opCode.GetByte(0x0F00, 8);
                    switch (opCode.L)
                    {
                        // FX07 Timer   Vx = get_delay()    Sets VX to the value of the delay timer.
                        case 0x07:
                            this.V[X] = this.DelayTimer;
                            this.PC += 2;
                            break;

                        // FX0A	KeyOp	Vx = get_key()	A key press is awaited, and then stored in VX. (Blocking Operation. All instruction halted until next key event)
                        case 0x0A:
                            for (byte i = 0; i < this.Key.Length; i++)
                            {
                                if (this.Key[i])
                                {
                                    this.V[X] = i;
                                    this.PC += 2;
                                    this.KeyUsed = true;
                                    break;
                                }
                            }

                            break;

                        // FX15 Timer   delay_timer(Vx) Sets the delay timer to VX.
                        case 0x15:
                            this.DelayTimer = this.V[X];
                            this.PC += 2;
                            break;

                        // FX18	Sound	sound_timer(Vx)	Sets the sound timer to VX.
                        case 0x18:
                            this.SoundTimer = this.V[X];
                            this.PC += 2;
                            break;

                        // FX1E	MEM	I +=Vx	Adds VX to I.[3]
                        case 0x1E:
                            this.I += this.V[X];
                            this.PC += 2;
                            break;

                        // Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented by a 4x5 font.
                        case 0x29:
                            this.I = (ushort)(this.V[X] * 5);
                            this.PC += 2;
                            break;

                        // Stores the Binary-coded decimal representation of VX at the addresses I, I plus 1, and I plus 2
                        case 0x33:
                            this.Mem[this.I] = (byte)(V[X] / 100);
                            this.Mem[this.I + 1] = (byte)(V[X] / 10 % 10);
                            this.Mem[this.I + 2] = (byte)(V[X] % 100 % 10);
                            this.PC += 2;
                            break;

                        // FX55	MEM	reg_dump(Vx,&I)	Stores V0 to VX (including VX) in memory starting at address I.[4]
                        case 0x55:
                            for (int i = 0; i <= X; i++)
                            {
                                this.Mem[this.I + i] = this.V[i];
                            }
                            this.PC += 2;
                            break;

                        // FX65	MEM	reg_load(Vx,&I)	Fills V0 to VX (including VX) with values from memory starting at address I.[4]
                        case 0x65:
                            for (int i = 0; i <= X; i++)
                            {
                                this.V[i] = this.Mem[this.I + i];
                            }
                            this.PC += 2;
                            break;
                    }
                    break;
            }

            if (this.DelayTimer > 0)
            {
                this.DelayTimer--;
            }

            if (this.SoundTimer > 0)
            {
                this.SoundTimer--;
            }
        }

        private static byte Subtract(uint a, uint b, out bool carry)
        {
            return Sum(a, (uint)(b * -1), out carry);
        }

        private static byte Sum(uint a, uint b, out bool carry)
        {
            uint result = a + b;
            uint carryBits = result & 0xFFFFFF00;
            carry = carryBits > 0;
            return (byte)(result & 0x000000FF);
        }

        [DebuggerDisplay("{StringValue}")]
        private class OpCode
        {
            public OpCode(byte h, byte l)
            {
                this.H = h;
                this.L = l;
                this.HH = (byte)(h >> 4);
                this.LL = (byte)(l & 0x0F);
            }

            public byte H;
            public byte L;
            public byte LL;
            public byte HH;

            public string StringValue => Convert.ToString(this.Value, 16);

            public ushort Value => (ushort)((H << 8) | L);

            public ushort GetShort(short mask, byte shiftRight)
            {
                var masked = this.Value & mask;
                var shifted = masked >> shiftRight;
                return (ushort)shifted;
            }

            public byte GetByte(short mask, byte shiftRight)
            {
                return (byte)this.GetShort(mask, shiftRight);
            }
        }
    }
}
